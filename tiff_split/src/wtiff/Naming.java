package wtiff;

/**
 * 分块图像的命名. (后期扩展可以加上分辨率等其他信息)
 * @author wang
 *
 */
public class Naming {
	public static final String Str_Format = "%s_%d_%d.%s";
	public static final String Str_Format_NoSuffix = "%s_%d_%d";
	/**
	 * 根据给定的Name & 行 & 列号，生成新的文件名
	 * @param args
	 */
	public static String GetName(String fileName, int column, int row){
		int suffix = fileName.indexOf(".");
		if (suffix != -1)
			return String.format(Str_Format,fileName.substring(0, suffix),column,row,
					fileName.substring(suffix+1));
		else
			return String.format(Str_Format_NoSuffix, fileName,column, row);
	}
	
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		String fileName = "KWW0012";
//		for(int i = 0,j = 10; i < 10; i++,j--){
//			System.out.println(GetName(fileName, i , j));
//		}
//	}

}
