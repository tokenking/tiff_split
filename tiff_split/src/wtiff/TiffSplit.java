package wtiff;
import java.nio.ByteBuffer;
import org.gdal.gdal.Driver;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconstConstants;
import org.gdal.gdal.Band;
/**
 * 给定原图像，将原图像分割为指定分辨率的子图像，写入指定的文件夹
 * @author wang
 *
 */
public class TiffSplit {
	public static final int Defual_Y = 256;
	public static final int Default_Y = 256;
	
	private String tiff;
	private String dir;
	private int targetX;
	private int targetY;
//	private Dataset source;
	
	public TiffSplit(String tiff, String dir, int x ,int y){
		this.tiff = tiff;
		this.dir = dir;
		this.targetX = x;
		this.targetY = y;
//		source = null;
	}
	
	private void init(){
		
	}
	
	/**
	 * 没有做边缘处理，多余的边缘已经去掉
	 * @return
	 */
	public void Split(){
		gdal.AllRegister();
		Dataset source = gdal.Open(tiff, gdalconstConstants.GA_ReadOnly);
		int xEnd = source.getRasterXSize() / targetX;
		int yEnd = source.GetRasterYSize() / targetY; 
		for (int r = 0; r < yEnd; r++){  //按列分
			for(int c = 0; c < xEnd; c++){
				ByteBuffer buff = ByteBuffer.allocateDirect(targetX * targetY * 3);
				int reValue =  source.ReadRaster_Direct(targetX*c, targetY*r, targetX, targetY, 
						targetX, targetY, gdalconstConstants.GDT_Byte, buff, new int[]{1,2,3});
				createTiff(source.GetDriver(),tiff,r,c,buff);
			}
		}
		
		source.delete();
	}
	
	private void createTiff(Driver dr, String name, int r ,int c, ByteBuffer buff){
		String fName = Naming.GetName(tiff, c, r);
		Dataset dest = dr.Create(fName, targetX, targetY, 3);
		dest.WriteRaster_Direct(0, 0, targetX, targetY, targetX, targetY, gdalconstConstants.GDT_Byte,
				buff, new int[]{1,2,3});
		dest.FlushCache();
		dest.delete();
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if (args.length < 2){
			Help();
			System.exit(0);
		}
		
		TiffSplit split = new TiffSplit(args[0], args[1], 256,256);
		split.Split();
		System.out.println("Completed split.");
	}
	
	public static void Help(){
		System.out.println("TiffSplit sourece dest_dir");
	}

}
